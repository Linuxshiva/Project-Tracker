import React, { Component } from 'react';
import './Header.css';
import logo from './../assests/images/logo/logo.png';
import NewProjectForm from '../pages/newProForm/newProForm';
import { withRouter } from 'react-router-dom'
import Cookies from 'universal-cookie';

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
              
        };

        this.openCreateProjectModel = this.openCreateProjectModel.bind(this);
      }

      openCreateProjectModel(evt){
         document.getElementById("newProjectLightbox").classList.toggle("open")
      }
    render() {
        return (
            <div>
                <div id="newProjectLightbox" className="lighbox">
                <div className="lightboxWrapper">
                    <span onClick={this.openCreateProjectModel} className="fa fa-times"></span>
                    <NewProjectForm />
                </div>
                
                </div>
                <header>
                    <div className="clear">
                        <div className="logo floatLeft">
                            <a href="javascript:void(0);">
                                <img src="https://images1.content-hci.com/hca-cont/india/img/hcindia_idp_logo_v1.png" alt="HC Logo" width='204' height='50' />
                            </a>
                        </div>
                        <div className="notification floatLeft">
                            <div className="floatLeft msgbox">
                                <a href="#">
                                    <i className="fa fa-envelope" aria-hidden="true"></i>
                                </a>
                                <span className="alert">
                                    <span>6</span>
                                </span>
                                <div className="dropDown">
                                    <div className="ddwrp">
                                        <i className="fa fa-caret-up" aria-hidden="true"></i>
                                        <div className="header">
                                            <h3>You have 3 messages</h3>
                                        </div>
                                        <div className="msgbody">
                                            <ul>
                                                <li className="clear">
                                                    <div className="floatLeft">
                                                        <img src="http://squaredesigns.net/jasmine/img/av1.png" alt="message1" />
                                                    </div>
                                                    <div className="floatRight">
                                                        <span className="who">Shiva sent you a message</span>
                                                        <span className="when">30 Mins ago</span>
                                                    </div>
                                                </li>
                                                <li className="clear">
                                                    <div className="floatLeft">
                                                        <img src="http://squaredesigns.net/jasmine/img/av1.png" alt="message1" />
                                                    </div>
                                                    <div className="floatRight">
                                                        <span className="who">Shiva sent you a message</span>
                                                        <span className="when">30 Mins ago</span>
                                                    </div>
                                                </li>
                                                <li className="clear">
                                                    <div className="floatLeft">
                                                        <img src="http://squaredesigns.net/jasmine/img/av1.png" alt="message1" />
                                                    </div>
                                                    <div className="floatRight">
                                                        <span className="who">Shiva sent you a message</span>
                                                        <span className="when">30 Mins ago</span>
                                                    </div>
                                                </li>
                                                <li className="clear">
                                                    <div className="floatLeft">
                                                        <img src="http://squaredesigns.net/jasmine/img/av1.png" alt="message1" />
                                                    </div>
                                                    <div className="floatRight">
                                                        <span className="who">Shiva sent you a message</span>
                                                        <span className="when">30 Mins ago</span>
                                                    </div>
                                                </li>

                                            </ul>
                                        </div>
                                        <div className="footer">
                                            <a href="#">
                                                Show all Messages
                                    </a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div className="floatLeft notbox">
                                <a href="#">
                                    <i className="fa fa-bell" aria-hidden="true"></i>
                                </a>
                                <span className="alert">
                                    <span>6</span>
                                </span>
                                <div className="dropDown">
                                    <div className="ddwrp">
                                        <i className="fa fa-caret-up" aria-hidden="true"></i>
                                        <div className="header">
                                            <h3>You have 3 messages</h3>
                                        </div>
                                        <div className="msgbody">
                                            <ul>
                                                <li className="clear">
                                                    <div className="floatLeft">
                                                        <img src="http://squaredesigns.net/jasmine/img/av1.png" alt="message1" />
                                                    </div>
                                                    <div className="floatRight">
                                                        <span className="who">Shiva sent you a message</span>
                                                        <span className="when">30 Mins ago</span>
                                                    </div>
                                                </li>
                                                <li className="clear">
                                                    <div className="floatLeft">
                                                        <img src="http://squaredesigns.net/jasmine/img/av1.png" alt="message1" />
                                                    </div>
                                                    <div className="floatRight">
                                                        <span className="who">Shiva sent you a message</span>
                                                        <span className="when">30 Mins ago</span>
                                                    </div>
                                                </li>
                                                <li className="clear">
                                                    <div className="floatLeft">
                                                        <img src="http://squaredesigns.net/jasmine/img/av1.png" alt="message1" />
                                                    </div>
                                                    <div className="floatRight">
                                                        <span className="who">Shiva sent you a message</span>
                                                        <span className="when">30 Mins ago</span>
                                                    </div>
                                                </li>
                                                <li className="clear">
                                                    <div className="floatLeft">
                                                        <img src="http://squaredesigns.net/jasmine/img/av1.png" alt="message1" />
                                                    </div>
                                                    <div className="floatRight">
                                                        <span className="who">Shiva sent you a message</span>
                                                        <span className="when">30 Mins ago</span>
                                                    </div>
                                                </li>

                                            </ul>
                                        </div>
                                        <div className="footer">
                                            <a href="#">
                                                Show all Messages
                                    </a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        {/* <div className="headerRightWrapper floatRight">
                <div className="user floatLeft">
                    <a href="#">
                        <img src="http://squaredesigns.net/jasmine/img/av1.png" alt="user_profile" />
                        <span>Shiva</span>
                    </a>
                </div>

            </div> */}

                        <div className="floatRight leadsection">

                        
                           {(() => {
                                if (this.props.Role == "Senior Manager") {
                                    return (
                                        <a href="javascript:void(0);" onClick={this.openCreateProjectModel}>Create Project</a>
                                    )
                                }
                            })()}
                        </div>
                    </div>
                </header>
            </div>
        );
    }
}

export default withRouter(Header);